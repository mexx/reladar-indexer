defmodule Indexer do
  @moduledoc """
  Documentation for Indexer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Indexer.hello
      :world

  """
  def hello do
    :world
  end
end
