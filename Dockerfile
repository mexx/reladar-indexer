FROM {{base_image}}\n
MAINTAINER {{maintainer}} <{{maintainer_email}}>\nCOPY . /app/ \n
WORKDIR /app \n
RUN bundle install \n
CMD foreman start